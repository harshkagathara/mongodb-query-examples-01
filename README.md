*Collections*:
<br>
 => CUSTOMER (cno, cust_name, cust_phone, location,gender).
 => ITEM (itemno, itemname, color, weight, expire_date, price, shop_name).
 => CUST_ITEM (cno, itemno, quantity_purchased, date_purchase)

 You need to make an apis for above.

1. Delete the items whose price is more than 50000.
2. Find the names of the customer who is located in same location as that of other customer.
3. Display the names of items which is black, white & brown in color.
4. Display the names of all the items whose names lies between ‘p’ and‘s’.
5. Find the item which is having less weight.
7. Count total number of items which is going to expire in next month
8. List all customers whose phone number starts with ‘99’
9. Display total value (qty*price) for all items.
10. List customer details who has purchased maximum number of items
11. Display total price item wise.
12. List name of items, customer details and qty purchased.

Note-:
When I run node application default data should be entered in all collections(5 records each
collections)
