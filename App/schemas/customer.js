const mongoose = require('mongoose');

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectId

const customerSchema = new mongoose.Schema({
    cno: {
        type: ObjectId
    },
    cust_name: {
        type: String
    },
    cust_phone: {
        type: String
    },
    location: {
        type: String
    },
    gender: {
        type: String
    },
});

const customerInfo = mongoose.model('customer', customerSchema);

module.exports = customerInfo;
