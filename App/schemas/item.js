const mongoose = require('mongoose');

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectId

const itemSchema = new mongoose.Schema({
    itemno: {
        type: ObjectId
    },
    itemname: {
        type: String
    },
    color: {
        type: String
    },
    weight: {
        type: Number
    },
    expire_date: {
        type: Date
    },
    price: {
        type: Number
    },
    shop_name: {
        type: String
    }
});

const itemInfo = mongoose.model('item', itemSchema);

module.exports = itemInfo;

