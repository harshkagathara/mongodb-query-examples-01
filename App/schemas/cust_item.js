const mongoose = require('mongoose');

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectId

const custitemSchema = new mongoose.Schema({
    cno: {
        type: ObjectId
    },
    itemno: {
        type: ObjectId
    },
    quantity_purchased: {
        type: Number
    },
    date_purchase: {
        type: Date
    }
});

const custitemSchemaInfo = mongoose.model('custitem', custitemSchema);
module.exports = custitemSchemaInfo;
