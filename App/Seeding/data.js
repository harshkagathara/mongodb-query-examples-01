const cust_item = require('../schemas/cust_item');
const customer = require('../schemas/customer');
const item = require('../schemas/item');
let ObjectID = require("mongodb").ObjectID;

require('../DB');

custItems();
customers();
items();

async function custItems(req, res) {
	cust_item.remove({}, function() {
		console.log('cust_item Colletion Cleared');
	});
	var Datas = [{
			"cno": ObjectID("60a651f23f1f3503347a801b"),
		    "itemno": ObjectID("62aca7498920a26c4ea428ea"),
			"quantity_purchased": 1,
            "date_purchase":"2021-05-27T08:41:26.216+00:00"
		},
		{
			"cno": ObjectID("60a651f23f1f3503347a401b"),
		    "itemno": ObjectID("62aca7498920a26c4ea427ea"),
			"quantity_purchased": 6,
            "date_purchase":"2021-06-27T08:41:26.216+00:00"
		},
		{
            "cno": ObjectID("60a651f23f1f3503347a401a"),
		    "itemno": ObjectID("62aca7498920a26c4ea417ea"),
			"quantity_purchased": 8,
            "date_purchase":"2021-09-27T08:41:26.216+00:00"
		},
		{
            "cno": ObjectID("60a651f23f1f3503347a501b"),
		    "cno": ObjectID("62aca7498920a26c4ea527ea"),
			"quantity_purchased": 89,
            "date_purchase":"2021-03-11T08:41:26.216+00:00"
		},
		{
            "cno": ObjectID("60a651f23f1f3503347a601b"),
		    "itemno": ObjectID("62aca7498920a26c4ea487ea"),
			"quantity_purchased": 11,
            "date_purchase":"2021-07-27T08:41:26.216+00:00"
		},
		{
			"cno": ObjectID("60a651f23f1f3503347a701b"),
		    "itemno": ObjectID("62aca7498920a26c4ea421ea"),
			"quantity_purchased": 22,
            "date_purchase":"2021-09-27T08:41:26.216+00:00"
		}
			
	];
	for(var i = 0; i < Datas.length; i++) {
		var newEvent = new cust_item(Datas[i]);
		console.log(newEvent);
		await newEvent.save();
	}
	console.log("Cust-item Colletion Seeded");
}

async function customers(req, res) {
	customer.remove({}, function() {
		console.log('customer Colletion Cleared');
	});
	var Datas = [{
			"cno": ObjectID("60a651f23f1f3503347a801b"),
		    "cust_name": "harsh kagathara",
			"cust_phone": "9773207180",
            "location":"Modpar",
            "gender":"male"
		},
		{
			"cno": ObjectID("60a651f23f1f3503347a701b"),
		    "cust_name": "Adarsh Bhoraniya",
			"cust_phone": "7778809323",
            "location":"halvad",
            "gender":"female"
		},
		{
            "cno": ObjectID("60a651f23f1f3503347a601b"),
		    "cust_name": "savan patel",
			"cust_phone": "9537360071",
            "location":"devliya",
            "gender":"female"
		},
		{
            "cno": ObjectID("60a651f23f1f3503347a501b"),
		    "cust_name": "kishan kotadiya",
			"cust_phone": "9824471906",
            "location":"lajai",
            "gender":"male"
		},
		{
            "cno": ObjectID("60a651f23f1f3503347a401b"),
		    "cust_name": "darshan makasana",
			"cust_phone": "9712212357",
            "location":"morbi",
            "gender":"female"
		},
		{
			"cno": ObjectID("60a651f23f1f3503347a401a"),
		    "cust_name": "yash kagathara",
			"cust_phone": "9974759685",
            "location":"morbi",
            "gender":"male"
		}
			
	];
	for(var i = 0; i < Datas.length; i++) {
		var newEvent = new customer(Datas[i]);
		console.log(newEvent);
		await newEvent.save();
	}
	console.log("customer Colletion Seeded");
}

async function items(req, res) {
	item.remove({}, function() {
		console.log('item Colletion Cleared');
	});
	var Datas = [{
			"itemno": ObjectID("62aca7498920a26c4ea428ea"),
		    "itemname": "laptop",
			"color": "red",
            "weight":3,
            "expire_date":"2021-05-27T08:41:26.216+00:00",
            "price": 150000,
            "shop_name":"eagle infotech"
		},
		{
			"itemno": ObjectID("62aca7498920a26c4ea421ea"),
		    "itemname": "samsung phone",
			"color": "white",
            "weight":2,
            "expire_date":"2021-01-27T08:41:26.216+00:00",
            "price": 32000,
            "shop_name":"ruhi cera"
		},
		{
            "itemno": ObjectID("62aca7498920a26c4ea417ea"),
		    "itemname": "mouse",
			"color": "black",
            "weight":1,
            "expire_date":"2022-07-27T08:41:26.216+00:00",
            "price": 200,
            "shop_name":"seav export"
		},
		{
            "itemno": ObjectID("62aca7498920a26c4ea527ea"),
		    "itemname": "iphone",
			"color": "brown",
            "weight": 5,
            "expire_date":"2021-01-07T08:41:26.216+00:00",
            "price": 85000,
            "shop_name":"e shop"
		},
		{
            "itemno": ObjectID("62aca7498920a26c4ea487ea"),
		    "itemname": "oneplus phone",
			"color": "dark black",
            "weight":9,
            "expire_date":"2021-01-28T08:41:26.216+00:00",
            "price": 45000,
            "shop_name":"apple infotech"
		},
		{
			"itemno": ObjectID("62aca7498920a26c4ea427ea"),
		    "itemname": "bag",
			"color": "yellow",
            "weight":7,
            "expire_date":"2022-01-27T08:41:26.216+00:00",
            "price": 5000,
            "shop_name":"infusion infotech"
		}
			
	];
	for(var i = 0; i < Datas.length; i++) {
		var newEvent = new item(Datas[i]);
		console.log(newEvent);
		await newEvent.save();
	}
	console.log("item Colletion Seeded");
}