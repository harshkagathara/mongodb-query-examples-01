module.exports = (app) => {

    const indexController = require('../controllers/index');

    app.get('/deletemore5k',  indexController.DeleteMore5k);
    app.get('/fineByColorName',  indexController.FineByColorName);
    app.get('/fineByProName',  indexController.FineByProName);
    app.get('/fineBylessWeight',  indexController.FineBylessWeight);
    app.get('/fineCountNextExpire',  indexController.FineCountNextExpire);
    app.get('/phoneNumStartBy99',  indexController.PhoneNumStartBy99);
    app.get('/displayAllItems',  indexController.DisplayAllItems);
    app.get('/listOfItemsCustomerQty',  indexController.ListOfItemsCustomerQty);
    app.get('/purchasedMaxItem',  indexController.PurchasedMaxItem);
    app.get('/sameLocation',  indexController.SameLocation);

}