const customer = require('../schemas/customer');
const item = require('../schemas/item');

class index {
	async deleteMore5k() {
		let promise = new Promise(async function(resolve, reject) {
			item.deleteOne({
				price: {
					$gt: 50000
				}
			}, (err, doc) => {
				if(!err) resolve(doc);
				else {
					console.error("Error during saving Category info : " + err);
				}
			});
		});
		return promise;
	}
	async fineByColorName() {
		let promise = new Promise(async function(resolve, reject) {
			item.find({
				color: ["black", "white", "brown"]
			}, (err, doc) => {
				if(!err) resolve(doc);
				else {
					console.error("Error during saving Category info : " + err);
				}
			});
		});
		return promise;
	}
	async fineByProName() {
		let promise = new Promise(async function(resolve, reject) {
			item.find({
				itemname: {
					$in: [/^s/i, /^p/]
				}
			}, (err, doc) => {
				if(!err) resolve(doc);
				else {
					console.error("Error during saving Category info : " + err);
				}
			});
		});
		return promise;
	}
	async fineBylessWeight() {
		let promise = new Promise(async function(resolve, reject) {
			let result = item.find().sort({
				weight: 1
			});
			resolve(result);
		});
		return promise;
	}
	async fineCountNextExpire() {
		let promise = new Promise(async function(resolve, reject) {
			let current = new Date();
			current.setMonth(current.getMonth() + 1);
			const lastDayOfNextMonth = new Date(current.getFullYear(), current.getMonth() + 1, 0);
			let result = item.find({
				"expire_date": {
					"$gte": new Date(current),
					"$lt": new Date(lastDayOfNextMonth)
				}
			})
			resolve(result);
		});
		return promise;
	}
	async phoneNumStartBy99() {
		let promise = new Promise(async function(resolve, reject) {
			customer.find({
				cust_phone: {
					$in: [/^99/i]
				}
			}, (err, doc) => {
				if(!err) resolve(doc);
				else {
					console.error("Error during saving Category info : " + err);
				}
			});
		});
		return promise;
	}
	async displayAllItems() {
		let query = [{
			$lookup: {
				from: "custitems",
				localField: "itemno",
				foreignField: "itemno",
				as: "itemData"
			}
		}, {
			$unwind: '$itemData'
		}, {
			$addFields: {
				"quantity_purchased": "$itemData.quantity_purchased",
			}
		}, {
			$project: {
				itemname: 1,
				color: 1,
				expire_date: 1,
				price: 1,
				quantity_purchased: 1,
				shop_name: 1,
				total: {
					$multiply: ["$price", "$quantity_purchased"]
				}
			}
		}];
		let promise = new Promise(async function(resolve, reject) {
			item.aggregate(query, function(err, data) {
				if(err) {
					console.log('Error during  : ' + err);
				}
				resolve(data);
			});
		});
		return promise;
	}
	async listOfItemsCustomerQty() {
		let query = [{
			$lookup: {
				from: "custitems",
				localField: "itemno",
				foreignField: "itemno",
				as: "custitemsData"
			}
		}, {
			$unwind: '$custitemsData'
		}, {
			"$lookup": {
				"localField": "custitemsData.cno",
				"from": "customers",
				"foreignField": "cno",
				"as": "customers"
			}
		}, {
			$addFields: {
				"cust_name": "$customers.cust_name",
				"cust_phone": "$customers.cust_phone",
				"location": "$customers.location",
				"gender": "$customers.gender",
				"qty": "$custitemsData.quantity_purchased",
			}
		}, {
			$project: {
				itemname: 1,
				color: 1,
				expire_date: 1,
				price: 1,
				cust_name: 1,
				cust_phone: 1,
				gender: 1,
				location: 1,
				qty: 1
			}
		}];
		let promise = new Promise(async function(resolve, reject) {
			item.aggregate(query, function(err, data) {
				if(err) {
					console.log('Error during  : ' + err);
				}
				resolve(data);
			});
		});
		return promise;
	}
	async purchasedMaxItem() {
		let query = [{
				$lookup: {
					from: "custitems",
					localField: "cno",
					foreignField: "cno",
					as: "custitemsData"
				}
			}, {
				$unwind: '$custitemsData'
			},
			{
				'$sort': {
					'custitemsData.quantity_purchased': -1
				}
			}
		];
		let promise = new Promise(async function(resolve, reject) {
			customer.aggregate(query, function(err, data) {
				if(err) {
					console.log('Error during  : ' + err);
				}
				resolve(data);
			});
		});
		return promise;
	}
	async sameLocation() {
        let query = [
            {
              '$group': {
                '_id': {
                  'location': '$location'
                }, 
                'count': {
                  '$sum': 1
                }
              }
            }, {
              '$sort': {
                'count': -1
              }
            }
          ]
		let promise = new Promise(async function(resolve, reject) {
            customer.aggregate(query, function(err, data) {
				if(err) {
					console.log('Error during  : ' + err);
				}
				resolve(data);
			});
		});
		return promise;
	}
}

let indexController = new index();
module.exports = indexController;

