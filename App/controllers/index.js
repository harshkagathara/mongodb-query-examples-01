let indexModel = require('../models/index');

exports.DeleteMore5k = async(req, res) => {
	let indexModelInfo1 = await indexModel.deleteMore5k();
	res.send(indexModelInfo1)
}
exports.FineByColorName = async(req, res) => {
	let indexModelInfo2 = await indexModel.fineByColorName();
	res.send(indexModelInfo2)
}
exports.FineByProName = async(req, res) => {
	let indexModelInfo3 = await indexModel.fineByProName();
	res.send(indexModelInfo3)
}
exports.FineBylessWeight = async(req, res) => {
	let indexModelInfo4 = await indexModel.fineBylessWeight();
	if(indexModelInfo4 != undefined && indexModelInfo4[0] != undefined) {
		res.send(indexModelInfo4[0])
	}
}
exports.FineCountNextExpire = async(req, res) => {
	let indexModelInfo5 = await indexModel.fineCountNextExpire();
	if(indexModelInfo5 != undefined) {
		res.send(indexModelInfo5)
	}
}
exports.PhoneNumStartBy99 = async(req, res) => {
	let indexModelInfo6 = await indexModel.phoneNumStartBy99();
	if(indexModelInfo6 != undefined) {
		res.send(indexModelInfo6)
	}
}
exports.DisplayAllItems = async(req, res) => {
	let indexModelInfo7 = await indexModel.displayAllItems();
	if(indexModelInfo7 != undefined) {
		res.send(indexModelInfo7)
	}
}
exports.ListOfItemsCustomerQty = async(req, res) => {
	let indexModelInfo7 = await indexModel.listOfItemsCustomerQty();
	if(indexModelInfo7 != undefined) {
		res.send(indexModelInfo7)
	}
}
exports.PurchasedMaxItem = async(req, res) => {
	let indexModelInfo7 = await indexModel.purchasedMaxItem();
	if(indexModelInfo7 != undefined && indexModelInfo7[0] != undefined) {
		res.send(indexModelInfo7[0])
	}
}
exports.SameLocation = async(req, res) => {
	let indexModelInfo7 = await indexModel.sameLocation();
	if(indexModelInfo7 != undefined && indexModelInfo7[0] != undefined) {
		res.send(indexModelInfo7[0])
	}
}