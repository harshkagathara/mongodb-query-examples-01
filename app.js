const express = require('express');
const bodyParser = require('body-parser');
require('dotenv').config();
const app = express();

require('./App/DB');

app.use('/',express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

require('./App/Routes/routes')(app);
require('./App/Seeding/data.js')
const Port = process.env.Port || 3000;
app.listen(Port, () => {
    console.log("Server Listing at :- 3000");
})